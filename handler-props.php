<?
AddEventHandler("main", "OnAdminListDisplay", array("mlifeHandlers","OnAdminListDisplayHandler"));
AddEventHandler("main", "OnBeforeProlog", array("mlifeHandlers","OnBeforePrologHandler"));
class mlifeHandlers{
	public static function OnBeforePrologHandler(){
		if (isset($_REQUEST['action_button']) && !isset($_REQUEST['action'])) {
			$_REQUEST['action'] = $_REQUEST['action_button'];
		}
		if (!isset($_REQUEST['action'])) {
			return;
		}
		if($_REQUEST['action']!=='mlife_properties' || !$_REQUEST['mlife_ib_prop'] || !is_array($_REQUEST['mlife_ib_prop_step2_val'])) return;
		$ids = $_REQUEST['ID'];
		if(!is_array($ids)) $ids[] = $_REQUEST['ID'];
		if(empty($ids)) return;
		//echo'<pre>';print_r($_REQUEST);echo'</pre>';die();
		\Bitrix\Main\Loader::includeModule('iblock');
		
		$propid = preg_replace("([^0-9])","",$_REQUEST['mlife_ib_prop']);
		$propValue = $_REQUEST['mlife_ib_prop_step2_val'];
		foreach($_REQUEST['mlife_ib_prop_step2_val'] as $v){
			if($v=='del') {
				$propValue = false;
				break;
			}
		}
		foreach($ids as $id){
			\CIBlockElement::SetPropertyValues($id, intval($_REQUEST['IBLOCK_ID']), $propValue, $propid);
			\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(intval($_REQUEST['IBLOCK_ID']), $id);
		}
		
	}
	public static function OnAdminListDisplayHandler(&$list) {
		$strCurPage = $GLOBALS['APPLICATION']->GetCurPage();
		if($strCurPage)
		$bElemPage = ($strCurPage=='/bitrix/admin/iblock_element_admin.php' ||
			$strCurPage=='/bitrix/admin/cat_product_admin.php'
		);
		if($bElemPage){
			\CJsCore::Init('jquery');
			$str = '<div id="mlife_ib_prop" style="display:none; "><select class="typeselect" name="mlife_ib_prop" id="mlife_ib_prop" onchange="setPropMlifeAjax(this.value,'.intval($_REQUEST['IBLOCK_ID']).')">';
			$properties = \CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>intval($_REQUEST['IBLOCK_ID'])));
			$str .= '<option value="-">выберите свойство</option>';
			while ($prop_fields = $properties->GetNext()){
				if($prop_fields['PROPERTY_TYPE'] == 'L') $str .= '<option value="'.$prop_fields['PROPERTY_TYPE'].($prop_fields['MULTIPLE']=='Y' ? 'M' : '').'_'.$prop_fields['ID'].'">'.$prop_fields['CODE'].' - '.$prop_fields['NAME'].'</option>';
			}
			$str .= '</select><div id="mlife_ib_prop_step2" style="display:inline-block;"></div></div>
			<script>
			function setPropMlife(val){
				BX.style(BX("mlife_ib_prop"), "display", ("mlife_properties" == val ? "inline-block" : "none"));
				//BX.style(BX("mlife_ib_prop"), "display", ("mlife_ib_prop_step2" == val ? "inline-block" : "none"));
			}
			</script>';
			$str .= '<script>
			function setPropMlifeAjax(val,iblock){
				if(val=="-"){
					$("#mlife_ib_prop_step2").html("");
					return;
				}
				var ar = val.split("_");
				console.log(ar);
				
				$.ajax({
					url: "/bitrix/admin/mlife_fraimework_ib_property.php",
					data: {prop_type:ar[0],prop_id:ar[1],iblock:iblock},
					dataType : "html",
					success: function (data, textStatus) {
						$("#mlife_ib_prop_step2").html(data);
					}
				});
				
				
			}
			</script>';
			$list->arActions['mlife_properties'] = 'Установить значение свойства';
			$list->arActions['mlife_setproperties'] = array('type' => 'html', 'value' => $str);
			$list->arActionsParams['select_onchange'] .= "setPropMlife(this.value);";
			
		}
	}
}
?>